from __future__ import unicode_literals
import numpy as np
from numpy.linalg import inv
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import pandas
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import SGDClassifier
from sklearn import linear_model
from sklearn import preprocessing
import math


class Diamonds:

    # Categorical features and their relations
    categories = {'cut': {'Fair': 0, 'Good': 1, 'Very Good': 2, 'Premium': 3, 'Ideal': 4},
                  'clarity': {'I1': 0, 'SI2': 1, 'SI1': 2, 'VS2': 3, 'VS1': 4, 'VVS2': 5, 'VVS1': 6, 'IF': 7},
                  'color': {'J': 0, 'I': 1, 'H': 2, 'G': 3, 'F': 4, 'E': 5, 'D': 6}
                  }

    def _init_(self):
        self.data = self.theta = self.X = self.y = self.valid = None
        self.show = False

    def import_data(self):
        filename = 'files/diamonds-train.csv'
        df = pandas.read_csv(filename)
        rows, cols = df.shape
        self.data = df.loc[:(0.8 * rows), :]
        self.valid = df.loc[(0.8 * rows):, :]

    def normalize(self):
        X = self.X
        self.X = (X - X.mean()) / (X.max() - X.min())

    def normalize_data(self):
        x = self.data.values  # returns a numpy array
        min_max_scaler = preprocessing.MinMaxScaler()
        x_scaled = min_max_scaler.fit_transform(x)
        self.data = pandas.DataFrame(x_scaled, columns=self.data.columns)

    def categ2numeric(self):
        self.data.replace(self.categories, inplace=True)
        self.valid.replace(self.categories, inplace=True)

    def plot(self, x_data, y_data):
        plt.plot(x_data[0], y_data[0])
        plt.xlabel(x_data[1])
        plt.ylabel(y_data[1])
        plt.show()

    def get_error(self, y, target):
        """
        Get the MAE and RMSE error based on target and gotten y
        """
        error_mae = error_rmse = [0] * len(y)
        error_mae = sum([abs(y[i] - target[i]) for i in range(len(y))]) / len(y)
        error_rmse = math.sqrt(sum([pow(y[i] - target[i], 2) for i in range(len(y))]) / len(y))

        print("[MAE] Error: ", error_mae)
        print("[RMSE] Error: ", error_rmse)

        return self


    def run(self):
        """
        Execute the model
        """
        target = self.valid.loc[:, 'price']
        X = self.valid.loc[:, self.data.columns != 'price']
        X.insert(0, 'x0', 1)
        X = X.values
        y = [0] * len(X)

        for k in range(len(X)):
            for i in range(len(self.theta)):
                y[k] = y[k] + (self.theta[i] * X[k][i])

        self.get_error(y, target.values)


    def normal_equation(self):
        """
        Calculate LR using normal equation
        """
        X = self.X
        y = self.y
        X_t = X.T
        self.theta = inv(X_t.dot(X)).dot(X_t).dot(y)
        print("[NE] Theta: ", self.theta)
        return self

    def gradient_descent(self, iterations=100, alpha=.00001):
        """
        Calculate LR using gradient descent
        """
        hist_cost = [0] * iterations
        hist_iterations = list(map(lambda x: x + 1, np.array(range(iterations))))

        rows, cols = self.data.shape

        x = self.X.values
        y = self.y.values
        theta = np.full(cols, 1)

        h = lambda X: (theta.T).dot(X)

        def cost(theta, m, h, x, y):
            s = 0
            for i in range(1, m): s = s + (h(x[i]) - y[i])**2
            return (1 / 2 * m) * s

        for it in range(iterations):
            gradient = 1 / rows * (x.T.dot(x.dot(theta) - y))
            theta = theta - alpha * gradient
            hist_cost[it] = cost(theta, rows, h, x, y)

        self.theta = theta

        print("[GD] Theta: ", self.theta)

        if(self.show): self.plot((hist_iterations, 'Iterations'), (hist_cost, 'Cost'))


    def sklearn_lm(self):
        """
        Calculate LR using Scikit-Learn
        """
        clf = linear_model.SGDRegressor()
        clf.fit(self.X, self.y.values.ravel())
        print(clf.coef_)

    def linear_regression(self, method, iterations=None, alpha=None, graphical=False):
        """
        Perform linear regression according to the method specified
        """
        self.show = graphical
        self.import_data()
        self.categ2numeric()
        self.X = self.data.loc[:, self.data.columns != 'price']
        self.y = self.data.loc[:, 'price']
        #self.normalize()
        self.X.insert(0, 'x0', 1)  # Add x0 = 0 element

        if method == 'NE':
            self.normal_equation()
        elif method == 'GD':
            self.gradient_descent(iterations, alpha)
        elif method == 'SKLEARN':
            self.sklearn_lm()
        else:
            print('Choose a method to perform linear regression.')

        return self


# Call methods
if __name__ == "__main__":
    Diamonds().linear_regression('GD', iterations=100, alpha=.00001, graphical=True).run()
    Diamonds().linear_regression('NE').run()
