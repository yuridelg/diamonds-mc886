# Trabalho 1 - Diamonds

## Getting Started

### Prerequisites

Os únicos pré requisitos para a execução do programa é uma versão Python instalada no sistema.

## Running

### Training

Para a execução de treinamento de modelos utilizando-se os metodos implementados, é necessário:

- Descomentar o trecho de código indicado no bloco "if name == "\__main__":":

```
######### DESCOMENTAR PARA ANALISE DE TREINAMENTOS ##########
...
...
######### DESCOMENTAR ATE AQUI PARA ANALISE DE TREINAMENTOS ##########
```

- Comentar o trecho de código indicado no bloco "if name == "\__main__":" para teste:

```
######### COMENTAR PARA ANALISE DE TREINAMENTOS ##########
...
...
######### COMENTAR ATE AQUI PARA ANALISE DE TREINAMENTOS ##########
```

- Executar o programa Python "__main.py__"

```
$ python main.py [train_file] [alpha] [iterations] [normal] [test_file]
```

onde os parâmetros são dados por:

- train_file: path para o arquivo de treino
- alpha: valor do alpha utilizado nos métodos Scikit Learn e Gradient Descent
- iterations: número de iterações utilizado nos métodos Scikit Learn e Gradient Descent
- normal: [0|1] booleano que indica se o conjunto de treino será normalizado
- test_file: path para o arquivo de teste, não utilizado para treinamento

Um exemplo de execução seria dado por:

```
$ python main.py files/diamonds-train.csv 0.00001 100000 1 files/diamonds-test.csv
```


### Test

Para a execução do teste a partir do melhor modelo encontrado, é necessário:

- Executar o programa Python "__main.py__"

```
$ python main.py [train_file] [alpha] [iterations] [normal] [test_file]
```

onde os parâmetros são dados por:

- train_file: path para o arquivo de treino
- alpha: valor não utilizado para o teste, inserir valor 0
- iterations: valor não utilizado para o teste, inserir valor 0
- normal: [0|1] booleano que indica se o conjunto de treino será normalizado
- test_file: path para o arquivo de teste

Um exemplo de execução seria dado por:

```
$ python main.py files/diamonds-train.csv 0.00001 100000 1 files/diamonds-test.csv
```

Como o melhor modelo já está definido, os valores de __alpha__ e __iterations__ não são utilizados.
