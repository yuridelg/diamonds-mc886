#################################################################################
# Trabalho 1 - 04/09/2018
#
# Aluno: Felipe S. P. Carvalho - RA: 146040
# Aluno: Yuri Catarino Delgado - RA: 158526
#
#################################################################################

import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn import linear_model
import sys

pd.options.mode.chained_assignment = None  # default='warn'

categories = {'cut': {'Fair': 0, 'Good': 1, 'Very Good': 2, 'Premium': 3, 'Ideal': 4},
              'clarity': {'I1': 0, 'SI2': 1, 'SI1': 2, 'VS2': 3, 'VS1': 4, 'VVS2': 5, 'VVS1': 6, 'IF': 7},
              'color': {'J': 0, 'I': 1, 'H': 2, 'G': 3, 'F': 4, 'E': 5, 'D': 6}
              }

def import_data(filename, use_volume=True, is_train=True):
    data = pd.read_csv(filename)
    if use_volume:
        dimensions = data[['x','y','z']]
        volume = dimensions.sum(axis=1)
        data = data.drop(['x', 'y', 'z'], axis=1)
        data.insert(4, 'dim', volume)
    rows, cols = data.shape
    if is_train:
        return data.loc[:(0.8 * rows), :], data.loc[(0.8 * rows):, :]
    else:
        return data


def categ2numeric(data):
    global categories
    data.replace(categories, inplace=True)


def get_datasets(data):
    X = data.loc[:, data.columns != 'price']
    y = data.loc[:, 'price']
    return X, y


def get_data_dimensions(data):
    rows, cols = data.shape
    return rows, cols


def plot(x_data, y_data):
    plt.plot(x_data[0], y_data[0])
    plt.xlabel(x_data[1])
    plt.ylabel(y_data[1])
    plt.show()


def normalize(df, min_val_arg=None, max_val_arg=None):
    max_val = {}
    min_val = {}

    for column in df:
        if min_val_arg is None and max_val_arg is None:
            # If min and max vals were not previously calculated
            max_val[column] = df[column].max()
            min_val[column] = df[column].min()
            df[column] = (df[column] - min_val[column]) / (max_val[column] - min_val[column])
        else:
            df[column] = (df[column] - min_val_arg[column]) / (max_val_arg[column] - min_val_arg[column])

    if min_val_arg is None and max_val_arg is None:
        return df, min_val, max_val
    else:
        return df, min_val_arg, max_val_arg


def normal_equation(X, y):
    X_t = X.T
    return np.linalg.inv(X_t.dot(X)).dot(X_t).dot(y)


def gradient_descent(X, Y, m, cols, iterations=1000, alpha=.0001):
    hist_cost = [0] * iterations
    hist_iterations = list(map(lambda x: x + 1, np.array(range(iterations))))

    x = X.values
    y = Y.values
    theta = np.ones(cols)

    h = lambda X, theta: x.dot(theta)

    for it in range(iterations):
        loss = h(X, theta) - y
        gradient = x.T.dot(loss) / m
        hist_cost[it] = np.sum(loss ** 2)
        theta -= alpha * gradient

    return theta, hist_iterations, hist_cost


def sklearn_lm(X, y, iterations, alpha):
    clf = linear_model.SGDRegressor(max_iter=iterations, eta0=alpha)
    clf.fit(X, y.values.ravel())
    theta = clf.coef_
    return theta


def run(X, target, theta):
    X = X.values
    y = [0] * len(X)

    for k in range(len(X)):
        for i in range(len(theta)):
            y[k] = y[k] + (theta[i] * X[k][i])

    get_error(y, target.values)


def get_error(y, target):
    error_mae = error_rmse = [0] * len(y)
    error_mae = sum([abs(y[i] - target[i]) for i in range(len(y))]) / len(y)
    error_rmse = math.sqrt(sum([pow(y[i] - target[i], 2) for i in range(len(y))]) / len(y))
    print("[MAE] Error: ", error_mae)
    print("[RMSE] Error: ", error_rmse)


if __name__ == "__main__":
    train_file = sys.argv[1]
    alpha = float(sys.argv[2])
    iterations = int(sys.argv[3])
    normal = int(sys.argv[4])
    test_file = sys.argv[5]

    ######### DESCOMENTAR PARA ANALISE DE TREINAMENTOS ##########
    # Train and validation sets
    # train, valid = import_data(filename=train_file)
    # categ2numeric(train)
    # X, y = get_datasets(train)
    # if normal:
    #     print("Normalizando dados...")
    #     X, min_val, max_val = normalize(X)
    # categ2numeric(valid)
    # X_valid, target = get_datasets(valid)
    # if normal:
    #     X_valid = normalize(X_valid, min_val, max_val)[0]
    # X.insert(0, 'x0', 1)
    # X_valid.insert(0, 'x0', 1)
    #
    # # Execute and validate NE
    # print("Normal Equation:")
    # theta = normal_equation(X, y)
    # print("[NE] Theta: ", theta)
    # run(X_valid, target, theta)
    #
    # # print("\nScikit Learn:")
    # # theta = sklearn_lm(X, y, iterations=iterations, alpha=alpha)
    # # print("[SKLearn] Theta: ", theta)
    # # run(X_valid, target, theta)
    #
    # print("\nGradient Descent:")
    # rows, cols = get_data_dimensions(train)
    # theta, hist_iterations, hist_cost = gradient_descent(X, y, rows, cols, alpha=alpha, iterations=iterations)
    # print("[GD] Theta: ", theta)
    # run(X_valid, target, theta)
    # plot((hist_iterations, 'Iterations'), (hist_cost, 'Cost'))
    ######### DESCOMENTAR ATE AQUI PARA ANALISE DE TREINAMENTOS ##########

    ######### COMENTAR PARA ANALISE DE TREINAMENTOS ##########
    # Test set
    train, valid = import_data(filename=train_file)
    categ2numeric(train)
    X, y = get_datasets(train)
    X, min_val, max_val = normalize(X)

    test = import_data(filename=test_file, is_train=False)
    categ2numeric(test)
    X_test, y_test = get_datasets(test)
    if normal:
        X_test = normalize(X_test, min_val, max_val)[0]
    X_test.insert(0, 'x0', 1)

    best_theta_gd =  [1038.63900502,  51305.17264294,    447.47304093,
       1931.00784594,   3522.89712464,  -8936.01831098,
       -2309.29005163,  -1769.31447856]

    best_theta_ne = [959.44055484, 51315.07225584, 471.22776422, 1940.98070046,
        3522.45520406, -8936.25555665, -2245.26415311, -1688.66756395]

    best_theta_sl =  [ -1487.09927138,  12525.16239813,    -94.30965576,
          -602.01596679,   291.31918588,  11672.86274422,
          -1033.10743697,     86.63900601]

    print("\nTest Gradient Descent:")
    run(X_test, y_test, best_theta_gd)
    print("\nTest Normal Equation:")
    run(X_test, y_test, best_theta_ne)
    print("\nTest SKLearn:")
    run(X_test, y_test, best_theta_sl)
    ######### COMENTAR ATE AQUI PARA ANALISE DE TREINAMENTOS ##########
